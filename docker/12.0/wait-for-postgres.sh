#!/bin/bash
# wait-for-postgres.sh

set -e


until PGPASSWORD=$RDS_PASSWORD psql -h "$RDS_HOSTNAME" -U "$RDS_USERNAME" -c '\q' "$RDS_DB_NAME"; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 2
done

>&2 echo "Postgres ready - resume startup"


echo HOST="$RDS_HOSTNAME" USER="$RDS_USERNAME" PASSWORD="$RDS_PASSWORD"
echo "-> ./entrypoint.sh -d " "$RDS_DB_NAME" "$@"

if [[ ! -v RDS_DB_NAME ]]; then
# for execution from pycharm debugger don't define RDS_DB_NAME env var
# we must pass database name on command line with -d option not using env var
# (we can't insert -d arg before $@ because of full command line replacement by pycharm)
echo "RDS_DB_NAME unset"
HOST="$RDS_HOSTNAME" USER="$RDS_USERNAME" PASSWORD="$RDS_PASSWORD" ./entrypoint.sh "$@"
else
# aws eb or docker-compose.yml case RDS_DB_NAME env var is set to db name
echo "RDS_DB_NAME=$RDS_DB_NAME"
HOST="$RDS_HOSTNAME" USER="$RDS_USERNAME" PASSWORD="$RDS_PASSWORD" ./entrypoint.sh -d "$RDS_DB_NAME" "$@"
fi
