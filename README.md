# Aroolla base Odoo - repository template -

_read-only repository_

## Prerequisities
Docker https://www.docker.com/get-started

## Getting started

- Use this repository to start a new Aroolla Odoo based project
 
- Then add your own modules and Aroolla dependencies to **addons** folder as git submodules

## get the sources
- clone the repository

``git clone --recurse-submodules git@gitlab.com:aroolla/aroolla_base_odoo.git``

*to retrieve submodules for already cloned repo run:*

 ``git submodule update --init --recursive``


## build and start:
- build and start
 
``docker-compose up -d``

*this will start the following:*
 

## containers:

- odoo: odoo 12 instance with base web module installed
- db: postgres database
- nginx: reverse proxy listening on host port 8069 
  (proxying web and longpolling odoo workers listening on containers ports 8069 and 8071)

_access:_
    
      http://localhost:8069
    
      default odoo admin login : admin/admin
      

- stop and delete containers (to start again from fresh data)

``docker-compose down``


